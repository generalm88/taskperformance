import * as React from 'react';
import { useEffect, useState, useCallback, useMemo } from "react";
import "./App.css";

const Button = React.memo(props => {
  console.count("render button");

  return <button {...props} style={{ backgroundColor: "lightgray" }} />;
}, (prevProps, nextProps) => prevProps !== nextProps)


const ListItem = React.memo(({ children }) => {
  console.count("render list item");

  return (
    <li>
      {children}
      <label style={{ fontSize: "smaller" }}>
        <input type="checkbox" />
        Add to cart
      </label>
    </li>
  );
})

const ProductsList = React.memo(({ isSortingDesc, searchString }) => {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    console.count("render fetch");

    fetch("https://reqres.in/api/products")
      .then((response) => response.json())
      .then((json) => {
        setProducts(json.data);
        MemoSortProduct()
      }
      );

  }, [searchString, isSortingDesc]);

  const MemoSortProduct = useCallback(
    () => {
      setProducts(products => products
        .filter((item) => item.name.includes(searchString))
        .sort((a, z) =>
          isSortingDesc
            ? z.name.localeCompare(a.name)
            : a.name.localeCompare(z.name)
        ))
    },
    [searchString, isSortingDesc],
  )

  return (
    <ul>
      {products.map((product) => {
        return <ListItem key={product.color}>{product.name}</ListItem>;
      })}
    </ul>
  )
})

const App = React.memo(() => {

  const [searchString, setSearchString] = useState("");
  const [isSortingDesc, setSortingDesc] = useState(false);
  const props = { isSortingDesc, searchString };

  console.count("render app");

  const SearchProductColor = useCallback(
    (e) => {
      setSearchString(e.target.value)
    }, [])


  const SortProductColor = useCallback(
    () => {
      setSortingDesc((value) => !value)
    }, [])

  const SearchListItem = React.memo(() => {
    return (
      <input
        type="search"
        value={searchString}
        onChange={SearchProductColor}
      />
    )
  }, [searchString])

  return (
    <div className="App">
      <SearchListItem />
      <Button onClick={SortProductColor}>
        Change sort direction
     </Button>
      <ProductsList {...props} />
    </div>
  );
})

export default App;
